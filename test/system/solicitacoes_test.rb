require "application_system_test_case"

class SolicitacoesTest < ApplicationSystemTestCase
  setup do
    @solicitaco = solicitacoes(:one)
  end

  test "visiting the index" do
    visit solicitacoes_url
    assert_selector "h1", text: "Solicitacoes"
  end

  test "creating a Solicitacoe" do
    visit solicitacoes_url
    click_on "New Solicitacoe"

    fill_in "Dataabertura", with: @solicitaco.dataabertura
    fill_in "Finalizado", with: @solicitaco.finalizado
    fill_in "Titulo", with: @solicitaco.titulo
    fill_in "User", with: @solicitaco.user_id
    fill_in "Userdestino", with: @solicitaco.userdestino
    click_on "Create Solicitacoe"

    assert_text "Solicitacoe was successfully created"
    click_on "Back"
  end

  test "updating a Solicitacoe" do
    visit solicitacoes_url
    click_on "Edit", match: :first

    fill_in "Dataabertura", with: @solicitaco.dataabertura
    fill_in "Finalizado", with: @solicitaco.finalizado
    fill_in "Titulo", with: @solicitaco.titulo
    fill_in "User", with: @solicitaco.user_id
    fill_in "Userdestino", with: @solicitaco.userdestino
    click_on "Update Solicitacoe"

    assert_text "Solicitacoe was successfully updated"
    click_on "Back"
  end

  test "destroying a Solicitacoe" do
    visit solicitacoes_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Solicitacoe was successfully destroyed"
  end
end

require "application_system_test_case"

class MovfinsTest < ApplicationSystemTestCase
  setup do
    @movfin = movfins(:one)
  end

  test "visiting the index" do
    visit movfins_url
    assert_selector "h1", text: "Movfins"
  end

  test "creating a Movfin" do
    visit movfins_url
    click_on "New Movfin"

    fill_in "Acao", with: @movfin.acao
    fill_in "Idmov", with: @movfin.idmov
    fill_in "Tipo", with: @movfin.tipo
    fill_in "Valor", with: @movfin.valor
    click_on "Create Movfin"

    assert_text "Movfin was successfully created"
    click_on "Back"
  end

  test "updating a Movfin" do
    visit movfins_url
    click_on "Edit", match: :first

    fill_in "Acao", with: @movfin.acao
    fill_in "Idmov", with: @movfin.idmov
    fill_in "Tipo", with: @movfin.tipo
    fill_in "Valor", with: @movfin.valor
    click_on "Update Movfin"

    assert_text "Movfin was successfully updated"
    click_on "Back"
  end

  test "destroying a Movfin" do
    visit movfins_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Movfin was successfully destroyed"
  end
end

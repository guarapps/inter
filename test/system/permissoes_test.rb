require "application_system_test_case"

class PermissoesTest < ApplicationSystemTestCase
  setup do
    @permisso = permissoes(:one)
  end

  test "visiting the index" do
    visit permissoes_url
    assert_selector "h1", text: "Permissoes"
  end

  test "creating a Permissoe" do
    visit permissoes_url
    click_on "New Permissoe"

    fill_in "Cargo", with: @permisso.cargo
    fill_in "Comiscorretor", with: @permisso.comiscorretor
    fill_in "Comisgerente", with: @permisso.comisgerente
    fill_in "Obseravacao", with: @permisso.obseravacao
    fill_in "User", with: @permisso.user_id
    click_on "Create Permissoe"

    assert_text "Permissoe was successfully created"
    click_on "Back"
  end

  test "updating a Permissoe" do
    visit permissoes_url
    click_on "Edit", match: :first

    fill_in "Cargo", with: @permisso.cargo
    fill_in "Comiscorretor", with: @permisso.comiscorretor
    fill_in "Comisgerente", with: @permisso.comisgerente
    fill_in "Obseravacao", with: @permisso.obseravacao
    fill_in "User", with: @permisso.user_id
    click_on "Update Permissoe"

    assert_text "Permissoe was successfully updated"
    click_on "Back"
  end

  test "destroying a Permissoe" do
    visit permissoes_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Permissoe was successfully destroyed"
  end
end

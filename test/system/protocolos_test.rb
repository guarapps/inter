require "application_system_test_case"

class ProtocolosTest < ApplicationSystemTestCase
  setup do
    @protocolo = protocolos(:one)
  end

  test "visiting the index" do
    visit protocolos_url
    assert_selector "h1", text: "Protocolos"
  end

  test "creating a Protocolo" do
    visit protocolos_url
    click_on "New Protocolo"

    fill_in "Banco", with: @protocolo.banco_id
    fill_in "Cliente", with: @protocolo.cliente_id
    fill_in "Prazo", with: @protocolo.prazo
    fill_in "Prestservico", with: @protocolo.prestservico
    fill_in "Valorcontrato", with: @protocolo.valorcontrato
    fill_in "Valorparcela", with: @protocolo.valorparcela
    click_on "Create Protocolo"

    assert_text "Protocolo was successfully created"
    click_on "Back"
  end

  test "updating a Protocolo" do
    visit protocolos_url
    click_on "Edit", match: :first

    fill_in "Banco", with: @protocolo.banco_id
    fill_in "Cliente", with: @protocolo.cliente_id
    fill_in "Prazo", with: @protocolo.prazo
    fill_in "Prestservico", with: @protocolo.prestservico
    fill_in "Valorcontrato", with: @protocolo.valorcontrato
    fill_in "Valorparcela", with: @protocolo.valorparcela
    click_on "Update Protocolo"

    assert_text "Protocolo was successfully updated"
    click_on "Back"
  end

  test "destroying a Protocolo" do
    visit protocolos_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Protocolo was successfully destroyed"
  end
end

require "application_system_test_case"

class ItematendimentosTest < ApplicationSystemTestCase
  setup do
    @itematendimento = itematendimentos(:one)
  end

  test "visiting the index" do
    visit itematendimentos_url
    assert_selector "h1", text: "Itematendimentos"
  end

  test "creating a Itematendimento" do
    visit itematendimentos_url
    click_on "New Itematendimento"

    fill_in "Atendimento", with: @itematendimento.atendimento_id
    fill_in "Numero1", with: @itematendimento.numero1
    fill_in "Numero2", with: @itematendimento.numero2
    fill_in "Obs", with: @itematendimento.obs
    fill_in "Quandoiraligar", with: @itematendimento.quandoiraligar
    fill_in "Quandoligou", with: @itematendimento.quandoligou
    fill_in "Sehaprotocolo", with: @itematendimento.sehaprotocolo
    click_on "Create Itematendimento"

    assert_text "Itematendimento was successfully created"
    click_on "Back"
  end

  test "updating a Itematendimento" do
    visit itematendimentos_url
    click_on "Edit", match: :first

    fill_in "Atendimento", with: @itematendimento.atendimento_id
    fill_in "Numero1", with: @itematendimento.numero1
    fill_in "Numero2", with: @itematendimento.numero2
    fill_in "Obs", with: @itematendimento.obs
    fill_in "Quandoiraligar", with: @itematendimento.quandoiraligar
    fill_in "Quandoligou", with: @itematendimento.quandoligou
    fill_in "Sehaprotocolo", with: @itematendimento.sehaprotocolo
    click_on "Update Itematendimento"

    assert_text "Itematendimento was successfully updated"
    click_on "Back"
  end

  test "destroying a Itematendimento" do
    visit itematendimentos_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Itematendimento was successfully destroyed"
  end
end

require "application_system_test_case"

class FeedbacktologsTest < ApplicationSystemTestCase
  setup do
    @feedbacktolog = feedbacktologs(:one)
  end

  test "visiting the index" do
    visit feedbacktologs_url
    assert_selector "h1", text: "Feedbacktologs"
  end

  test "creating a Feedbacktolog" do
    visit feedbacktologs_url
    click_on "New Feedbacktolog"

    fill_in "Lido", with: @feedbacktolog.lido
    fill_in "Log", with: @feedbacktolog.log_id
    fill_in "Obs", with: @feedbacktolog.obs
    fill_in "User", with: @feedbacktolog.user_id
    click_on "Create Feedbacktolog"

    assert_text "Feedbacktolog was successfully created"
    click_on "Back"
  end

  test "updating a Feedbacktolog" do
    visit feedbacktologs_url
    click_on "Edit", match: :first

    fill_in "Lido", with: @feedbacktolog.lido
    fill_in "Log", with: @feedbacktolog.log_id
    fill_in "Obs", with: @feedbacktolog.obs
    fill_in "User", with: @feedbacktolog.user_id
    click_on "Update Feedbacktolog"

    assert_text "Feedbacktolog was successfully updated"
    click_on "Back"
  end

  test "destroying a Feedbacktolog" do
    visit feedbacktologs_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Feedbacktolog was successfully destroyed"
  end
end

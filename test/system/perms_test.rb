require "application_system_test_case"

class PermsTest < ApplicationSystemTestCase
  setup do
    @perm = perms(:one)
  end

  test "visiting the index" do
    visit perms_url
    assert_selector "h1", text: "Perms"
  end

  test "creating a Perm" do
    visit perms_url
    click_on "New Perm"

    fill_in "Cargo", with: @perm.cargo
    fill_in "Comissaocorretor", with: @perm.comissaocorretor
    fill_in "Comissaogerente", with: @perm.comissaogerente
    fill_in "Idgerente", with: @perm.idgerente
    fill_in "User", with: @perm.user_id
    click_on "Create Perm"

    assert_text "Perm was successfully created"
    click_on "Back"
  end

  test "updating a Perm" do
    visit perms_url
    click_on "Edit", match: :first

    fill_in "Cargo", with: @perm.cargo
    fill_in "Comissaocorretor", with: @perm.comissaocorretor
    fill_in "Comissaogerente", with: @perm.comissaogerente
    fill_in "Idgerente", with: @perm.idgerente
    fill_in "User", with: @perm.user_id
    click_on "Update Perm"

    assert_text "Perm was successfully updated"
    click_on "Back"
  end

  test "destroying a Perm" do
    visit perms_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Perm was successfully destroyed"
  end
end

require "application_system_test_case"

class DadosTest < ApplicationSystemTestCase
  setup do
    @dado = dados(:one)
  end

  test "visiting the index" do
    visit dados_url
    assert_selector "h1", text: "Dados"
  end

  test "creating a Dado" do
    visit dados_url
    click_on "New Dado"

    fill_in "Agencia", with: @dado.agencia
    fill_in "Conta", with: @dado.conta
    fill_in "Cpf", with: @dado.cpf
    fill_in "Datainicio", with: @dado.datainicio
    fill_in "Datasaida", with: @dado.datasaida
    fill_in "Telefone", with: @dado.telefone
    fill_in "User", with: @dado.user_id
    click_on "Create Dado"

    assert_text "Dado was successfully created"
    click_on "Back"
  end

  test "updating a Dado" do
    visit dados_url
    click_on "Edit", match: :first

    fill_in "Agencia", with: @dado.agencia
    fill_in "Conta", with: @dado.conta
    fill_in "Cpf", with: @dado.cpf
    fill_in "Datainicio", with: @dado.datainicio
    fill_in "Datasaida", with: @dado.datasaida
    fill_in "Telefone", with: @dado.telefone
    fill_in "User", with: @dado.user_id
    click_on "Update Dado"

    assert_text "Dado was successfully updated"
    click_on "Back"
  end

  test "destroying a Dado" do
    visit dados_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Dado was successfully destroyed"
  end
end

require "application_system_test_case"

class ItematendimentoclientesTest < ApplicationSystemTestCase
  setup do
    @itematendimentocliente = itematendimentoclientes(:one)
  end

  test "visiting the index" do
    visit itematendimentoclientes_url
    assert_selector "h1", text: "Itematendimentoclientes"
  end

  test "creating a Itematendimentocliente" do
    visit itematendimentoclientes_url
    click_on "New Itematendimentocliente"

    fill_in "Cliente", with: @itematendimentocliente.cliente_id
    fill_in "Numero", with: @itematendimentocliente.numero
    fill_in "Numero1", with: @itematendimentocliente.numero1
    fill_in "Numero2", with: @itematendimentocliente.numero2
    fill_in "Obs", with: @itematendimentocliente.obs
    fill_in "Quandoiraligar", with: @itematendimentocliente.quandoiraligar
    fill_in "Quandoligou", with: @itematendimentocliente.quandoligou
    fill_in "User", with: @itematendimentocliente.user_id
    click_on "Create Itematendimentocliente"

    assert_text "Itematendimentocliente was successfully created"
    click_on "Back"
  end

  test "updating a Itematendimentocliente" do
    visit itematendimentoclientes_url
    click_on "Edit", match: :first

    fill_in "Cliente", with: @itematendimentocliente.cliente_id
    fill_in "Numero", with: @itematendimentocliente.numero
    fill_in "Numero1", with: @itematendimentocliente.numero1
    fill_in "Numero2", with: @itematendimentocliente.numero2
    fill_in "Obs", with: @itematendimentocliente.obs
    fill_in "Quandoiraligar", with: @itematendimentocliente.quandoiraligar
    fill_in "Quandoligou", with: @itematendimentocliente.quandoligou
    fill_in "User", with: @itematendimentocliente.user_id
    click_on "Update Itematendimentocliente"

    assert_text "Itematendimentocliente was successfully updated"
    click_on "Back"
  end

  test "destroying a Itematendimentocliente" do
    visit itematendimentoclientes_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Itematendimentocliente was successfully destroyed"
  end
end

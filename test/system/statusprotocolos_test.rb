require "application_system_test_case"

class StatusprotocolosTest < ApplicationSystemTestCase
  setup do
    @statusprotocolo = statusprotocolos(:one)
  end

  test "visiting the index" do
    visit statusprotocolos_url
    assert_selector "h1", text: "Statusprotocolos"
  end

  test "creating a Statusprotocolo" do
    visit statusprotocolos_url
    click_on "New Statusprotocolo"

    fill_in "Obs", with: @statusprotocolo.obs
    fill_in "Protocolo", with: @statusprotocolo.protocolo_id
    fill_in "Status", with: @statusprotocolo.status
    click_on "Create Statusprotocolo"

    assert_text "Statusprotocolo was successfully created"
    click_on "Back"
  end

  test "updating a Statusprotocolo" do
    visit statusprotocolos_url
    click_on "Edit", match: :first

    fill_in "Obs", with: @statusprotocolo.obs
    fill_in "Protocolo", with: @statusprotocolo.protocolo_id
    fill_in "Status", with: @statusprotocolo.status
    click_on "Update Statusprotocolo"

    assert_text "Statusprotocolo was successfully updated"
    click_on "Back"
  end

  test "destroying a Statusprotocolo" do
    visit statusprotocolos_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Statusprotocolo was successfully destroyed"
  end
end

require "application_system_test_case"

class ItemsolicsTest < ApplicationSystemTestCase
  setup do
    @itemsolic = itemsolics(:one)
  end

  test "visiting the index" do
    visit itemsolics_url
    assert_selector "h1", text: "Itemsolics"
  end

  test "creating a Itemsolic" do
    visit itemsolics_url
    click_on "New Itemsolic"

    fill_in "Obs", with: @itemsolic.obs
    fill_in "Solicitacoe", with: @itemsolic.solicitacoe_id
    fill_in "User", with: @itemsolic.user_id
    click_on "Create Itemsolic"

    assert_text "Itemsolic was successfully created"
    click_on "Back"
  end

  test "updating a Itemsolic" do
    visit itemsolics_url
    click_on "Edit", match: :first

    fill_in "Obs", with: @itemsolic.obs
    fill_in "Solicitacoe", with: @itemsolic.solicitacoe_id
    fill_in "User", with: @itemsolic.user_id
    click_on "Update Itemsolic"

    assert_text "Itemsolic was successfully updated"
    click_on "Back"
  end

  test "destroying a Itemsolic" do
    visit itemsolics_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Itemsolic was successfully destroyed"
  end
end

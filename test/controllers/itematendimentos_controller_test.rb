require 'test_helper'

class ItematendimentosControllerTest < ActionDispatch::IntegrationTest
  setup do
    @itematendimento = itematendimentos(:one)
  end

  test "should get index" do
    get itematendimentos_url
    assert_response :success
  end

  test "should get new" do
    get new_itematendimento_url
    assert_response :success
  end

  test "should create itematendimento" do
    assert_difference('Itematendimento.count') do
      post itematendimentos_url, params: { itematendimento: { atendimento_id: @itematendimento.atendimento_id, numero1: @itematendimento.numero1, numero2: @itematendimento.numero2, obs: @itematendimento.obs, quandoiraligar: @itematendimento.quandoiraligar, quandoligou: @itematendimento.quandoligou, sehaprotocolo: @itematendimento.sehaprotocolo } }
    end

    assert_redirected_to itematendimento_url(Itematendimento.last)
  end

  test "should show itematendimento" do
    get itematendimento_url(@itematendimento)
    assert_response :success
  end

  test "should get edit" do
    get edit_itematendimento_url(@itematendimento)
    assert_response :success
  end

  test "should update itematendimento" do
    patch itematendimento_url(@itematendimento), params: { itematendimento: { atendimento_id: @itematendimento.atendimento_id, numero1: @itematendimento.numero1, numero2: @itematendimento.numero2, obs: @itematendimento.obs, quandoiraligar: @itematendimento.quandoiraligar, quandoligou: @itematendimento.quandoligou, sehaprotocolo: @itematendimento.sehaprotocolo } }
    assert_redirected_to itematendimento_url(@itematendimento)
  end

  test "should destroy itematendimento" do
    assert_difference('Itematendimento.count', -1) do
      delete itematendimento_url(@itematendimento)
    end

    assert_redirected_to itematendimentos_url
  end
end

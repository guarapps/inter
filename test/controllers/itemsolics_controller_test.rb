require 'test_helper'

class ItemsolicsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @itemsolic = itemsolics(:one)
  end

  test "should get index" do
    get itemsolics_url
    assert_response :success
  end

  test "should get new" do
    get new_itemsolic_url
    assert_response :success
  end

  test "should create itemsolic" do
    assert_difference('Itemsolic.count') do
      post itemsolics_url, params: { itemsolic: { obs: @itemsolic.obs, solicitacoe_id: @itemsolic.solicitacoe_id, user_id: @itemsolic.user_id } }
    end

    assert_redirected_to itemsolic_url(Itemsolic.last)
  end

  test "should show itemsolic" do
    get itemsolic_url(@itemsolic)
    assert_response :success
  end

  test "should get edit" do
    get edit_itemsolic_url(@itemsolic)
    assert_response :success
  end

  test "should update itemsolic" do
    patch itemsolic_url(@itemsolic), params: { itemsolic: { obs: @itemsolic.obs, solicitacoe_id: @itemsolic.solicitacoe_id, user_id: @itemsolic.user_id } }
    assert_redirected_to itemsolic_url(@itemsolic)
  end

  test "should destroy itemsolic" do
    assert_difference('Itemsolic.count', -1) do
      delete itemsolic_url(@itemsolic)
    end

    assert_redirected_to itemsolics_url
  end
end

require 'test_helper'

class SolicitacoesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @solicitaco = solicitacoes(:one)
  end

  test "should get index" do
    get solicitacoes_url
    assert_response :success
  end

  test "should get new" do
    get new_solicitaco_url
    assert_response :success
  end

  test "should create solicitaco" do
    assert_difference('Solicitacoe.count') do
      post solicitacoes_url, params: { solicitaco: { dataabertura: @solicitaco.dataabertura, finalizado: @solicitaco.finalizado, titulo: @solicitaco.titulo, user_id: @solicitaco.user_id, userdestino: @solicitaco.userdestino } }
    end

    assert_redirected_to solicitaco_url(Solicitacoe.last)
  end

  test "should show solicitaco" do
    get solicitaco_url(@solicitaco)
    assert_response :success
  end

  test "should get edit" do
    get edit_solicitaco_url(@solicitaco)
    assert_response :success
  end

  test "should update solicitaco" do
    patch solicitaco_url(@solicitaco), params: { solicitaco: { dataabertura: @solicitaco.dataabertura, finalizado: @solicitaco.finalizado, titulo: @solicitaco.titulo, user_id: @solicitaco.user_id, userdestino: @solicitaco.userdestino } }
    assert_redirected_to solicitaco_url(@solicitaco)
  end

  test "should destroy solicitaco" do
    assert_difference('Solicitacoe.count', -1) do
      delete solicitaco_url(@solicitaco)
    end

    assert_redirected_to solicitacoes_url
  end
end

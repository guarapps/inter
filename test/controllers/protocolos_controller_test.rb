require 'test_helper'

class ProtocolosControllerTest < ActionDispatch::IntegrationTest
  setup do
    @protocolo = protocolos(:one)
  end

  test "should get index" do
    get protocolos_url
    assert_response :success
  end

  test "should get new" do
    get new_protocolo_url
    assert_response :success
  end

  test "should create protocolo" do
    assert_difference('Protocolo.count') do
      post protocolos_url, params: { protocolo: { banco_id: @protocolo.banco_id, cliente_id: @protocolo.cliente_id, prazo: @protocolo.prazo, prestservico: @protocolo.prestservico, valorcontrato: @protocolo.valorcontrato, valorparcela: @protocolo.valorparcela } }
    end

    assert_redirected_to protocolo_url(Protocolo.last)
  end

  test "should show protocolo" do
    get protocolo_url(@protocolo)
    assert_response :success
  end

  test "should get edit" do
    get edit_protocolo_url(@protocolo)
    assert_response :success
  end

  test "should update protocolo" do
    patch protocolo_url(@protocolo), params: { protocolo: { banco_id: @protocolo.banco_id, cliente_id: @protocolo.cliente_id, prazo: @protocolo.prazo, prestservico: @protocolo.prestservico, valorcontrato: @protocolo.valorcontrato, valorparcela: @protocolo.valorparcela } }
    assert_redirected_to protocolo_url(@protocolo)
  end

  test "should destroy protocolo" do
    assert_difference('Protocolo.count', -1) do
      delete protocolo_url(@protocolo)
    end

    assert_redirected_to protocolos_url
  end
end

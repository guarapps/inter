require 'test_helper'

class FeedbacktologsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @feedbacktolog = feedbacktologs(:one)
  end

  test "should get index" do
    get feedbacktologs_url
    assert_response :success
  end

  test "should get new" do
    get new_feedbacktolog_url
    assert_response :success
  end

  test "should create feedbacktolog" do
    assert_difference('Feedbacktolog.count') do
      post feedbacktologs_url, params: { feedbacktolog: { lido: @feedbacktolog.lido, log_id: @feedbacktolog.log_id, obs: @feedbacktolog.obs, user_id: @feedbacktolog.user_id } }
    end

    assert_redirected_to feedbacktolog_url(Feedbacktolog.last)
  end

  test "should show feedbacktolog" do
    get feedbacktolog_url(@feedbacktolog)
    assert_response :success
  end

  test "should get edit" do
    get edit_feedbacktolog_url(@feedbacktolog)
    assert_response :success
  end

  test "should update feedbacktolog" do
    patch feedbacktolog_url(@feedbacktolog), params: { feedbacktolog: { lido: @feedbacktolog.lido, log_id: @feedbacktolog.log_id, obs: @feedbacktolog.obs, user_id: @feedbacktolog.user_id } }
    assert_redirected_to feedbacktolog_url(@feedbacktolog)
  end

  test "should destroy feedbacktolog" do
    assert_difference('Feedbacktolog.count', -1) do
      delete feedbacktolog_url(@feedbacktolog)
    end

    assert_redirected_to feedbacktologs_url
  end
end

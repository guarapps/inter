require 'test_helper'

class PermissoesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @permisso = permissoes(:one)
  end

  test "should get index" do
    get permissoes_url
    assert_response :success
  end

  test "should get new" do
    get new_permisso_url
    assert_response :success
  end

  test "should create permisso" do
    assert_difference('Permissoe.count') do
      post permissoes_url, params: { permisso: { cargo: @permisso.cargo, comiscorretor: @permisso.comiscorretor, comisgerente: @permisso.comisgerente, obseravacao: @permisso.obseravacao, user_id: @permisso.user_id } }
    end

    assert_redirected_to permisso_url(Permissoe.last)
  end

  test "should show permisso" do
    get permisso_url(@permisso)
    assert_response :success
  end

  test "should get edit" do
    get edit_permisso_url(@permisso)
    assert_response :success
  end

  test "should update permisso" do
    patch permisso_url(@permisso), params: { permisso: { cargo: @permisso.cargo, comiscorretor: @permisso.comiscorretor, comisgerente: @permisso.comisgerente, obseravacao: @permisso.obseravacao, user_id: @permisso.user_id } }
    assert_redirected_to permisso_url(@permisso)
  end

  test "should destroy permisso" do
    assert_difference('Permissoe.count', -1) do
      delete permisso_url(@permisso)
    end

    assert_redirected_to permissoes_url
  end
end

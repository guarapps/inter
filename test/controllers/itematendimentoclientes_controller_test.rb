require 'test_helper'

class ItematendimentoclientesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @itematendimentocliente = itematendimentoclientes(:one)
  end

  test "should get index" do
    get itematendimentoclientes_url
    assert_response :success
  end

  test "should get new" do
    get new_itematendimentocliente_url
    assert_response :success
  end

  test "should create itematendimentocliente" do
    assert_difference('Itematendimentocliente.count') do
      post itematendimentoclientes_url, params: { itematendimentocliente: { cliente_id: @itematendimentocliente.cliente_id, numero: @itematendimentocliente.numero, numero1: @itematendimentocliente.numero1, numero2: @itematendimentocliente.numero2, obs: @itematendimentocliente.obs, quandoiraligar: @itematendimentocliente.quandoiraligar, quandoligou: @itematendimentocliente.quandoligou, user_id: @itematendimentocliente.user_id } }
    end

    assert_redirected_to itematendimentocliente_url(Itematendimentocliente.last)
  end

  test "should show itematendimentocliente" do
    get itematendimentocliente_url(@itematendimentocliente)
    assert_response :success
  end

  test "should get edit" do
    get edit_itematendimentocliente_url(@itematendimentocliente)
    assert_response :success
  end

  test "should update itematendimentocliente" do
    patch itematendimentocliente_url(@itematendimentocliente), params: { itematendimentocliente: { cliente_id: @itematendimentocliente.cliente_id, numero: @itematendimentocliente.numero, numero1: @itematendimentocliente.numero1, numero2: @itematendimentocliente.numero2, obs: @itematendimentocliente.obs, quandoiraligar: @itematendimentocliente.quandoiraligar, quandoligou: @itematendimentocliente.quandoligou, user_id: @itematendimentocliente.user_id } }
    assert_redirected_to itematendimentocliente_url(@itematendimentocliente)
  end

  test "should destroy itematendimentocliente" do
    assert_difference('Itematendimentocliente.count', -1) do
      delete itematendimentocliente_url(@itematendimentocliente)
    end

    assert_redirected_to itematendimentoclientes_url
  end
end

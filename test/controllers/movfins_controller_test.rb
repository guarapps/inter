require 'test_helper'

class MovfinsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @movfin = movfins(:one)
  end

  test "should get index" do
    get movfins_url
    assert_response :success
  end

  test "should get new" do
    get new_movfin_url
    assert_response :success
  end

  test "should create movfin" do
    assert_difference('Movfin.count') do
      post movfins_url, params: { movfin: { acao: @movfin.acao, idmov: @movfin.idmov, tipo: @movfin.tipo, valor: @movfin.valor } }
    end

    assert_redirected_to movfin_url(Movfin.last)
  end

  test "should show movfin" do
    get movfin_url(@movfin)
    assert_response :success
  end

  test "should get edit" do
    get edit_movfin_url(@movfin)
    assert_response :success
  end

  test "should update movfin" do
    patch movfin_url(@movfin), params: { movfin: { acao: @movfin.acao, idmov: @movfin.idmov, tipo: @movfin.tipo, valor: @movfin.valor } }
    assert_redirected_to movfin_url(@movfin)
  end

  test "should destroy movfin" do
    assert_difference('Movfin.count', -1) do
      delete movfin_url(@movfin)
    end

    assert_redirected_to movfins_url
  end
end

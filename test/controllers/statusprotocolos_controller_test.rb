require 'test_helper'

class StatusprotocolosControllerTest < ActionDispatch::IntegrationTest
  setup do
    @statusprotocolo = statusprotocolos(:one)
  end

  test "should get index" do
    get statusprotocolos_url
    assert_response :success
  end

  test "should get new" do
    get new_statusprotocolo_url
    assert_response :success
  end

  test "should create statusprotocolo" do
    assert_difference('Statusprotocolo.count') do
      post statusprotocolos_url, params: { statusprotocolo: { obs: @statusprotocolo.obs, protocolo_id: @statusprotocolo.protocolo_id, status: @statusprotocolo.status } }
    end

    assert_redirected_to statusprotocolo_url(Statusprotocolo.last)
  end

  test "should show statusprotocolo" do
    get statusprotocolo_url(@statusprotocolo)
    assert_response :success
  end

  test "should get edit" do
    get edit_statusprotocolo_url(@statusprotocolo)
    assert_response :success
  end

  test "should update statusprotocolo" do
    patch statusprotocolo_url(@statusprotocolo), params: { statusprotocolo: { obs: @statusprotocolo.obs, protocolo_id: @statusprotocolo.protocolo_id, status: @statusprotocolo.status } }
    assert_redirected_to statusprotocolo_url(@statusprotocolo)
  end

  test "should destroy statusprotocolo" do
    assert_difference('Statusprotocolo.count', -1) do
      delete statusprotocolo_url(@statusprotocolo)
    end

    assert_redirected_to statusprotocolos_url
  end
end

class AddUserToItematendimento < ActiveRecord::Migration[6.0]
  def change
    add_reference :itematendimentos, :user, null: true, foreign_key: true
  end
end

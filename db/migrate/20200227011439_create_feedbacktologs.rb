class CreateFeedbacktologs < ActiveRecord::Migration[6.0]
  def change
    create_table :feedbacktologs do |t|
      t.text :obs
      t.references :user, null: false, foreign_key: true
      t.references :log, null: false, foreign_key: true
      t.string :lido

      t.timestamps
    end
  end
end

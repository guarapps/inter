class CreateProtocolos < ActiveRecord::Migration[6.0]
  def change
    create_table :protocolos do |t|
      t.numeric :valorparcela
      t.integer :prazo
      t.numeric :valorcontrato
      t.numeric :prestservico
      t.references :cliente, null: false, foreign_key: true
      t.references :banco, null: false, foreign_key: true

      t.timestamps
    end
  end
end

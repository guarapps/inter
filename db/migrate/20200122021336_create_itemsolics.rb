class CreateItemsolics < ActiveRecord::Migration[6.0]
  def change
    create_table :itemsolics do |t|
      t.references :solicitacoe, null: false, foreign_key: true
      t.text :obs
      t.references :user, null: false, foreign_key: true

      t.timestamps
    end
  end
end

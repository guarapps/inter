class AddUserToCliente < ActiveRecord::Migration[6.0]
  def change
    add_reference :clientes, :user, null: true, foreign_key: true
  end
end

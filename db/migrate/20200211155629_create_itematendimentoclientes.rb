class CreateItematendimentoclientes < ActiveRecord::Migration[6.0]
  def change
    create_table :itematendimentoclientes do |t|
      t.string :numero
      t.references :user, null: false, foreign_key: true
      t.date :quandoligou
      t.date :quandoiraligar
      t.string :numero1
      t.string :numero2
      t.text :obs
      t.references :cliente, null: false, foreign_key: true

      t.timestamps
    end
  end
end

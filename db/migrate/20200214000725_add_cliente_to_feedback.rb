class AddClienteToFeedback < ActiveRecord::Migration[6.0]
  def change
    add_reference :feedbacks, :cliente, null: true, foreign_key: true
  end
end

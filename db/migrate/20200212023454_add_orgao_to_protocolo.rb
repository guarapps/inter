class AddOrgaoToProtocolo < ActiveRecord::Migration[6.0]
  def change
    add_column :protocolos, :orgao, :string
  end
end

class CreateItematendimentos < ActiveRecord::Migration[6.0]
  def change
    create_table :itematendimentos do |t|
      t.references :atendimento, null: false, foreign_key: true
      t.string :quandoligou
      t.string :quandoiraligar
      t.string :numero1
      t.string :numero2
      t.text :obs
      t.string :sehaprotocolo

      t.timestamps
    end
  end
end

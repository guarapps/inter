class CreatePerms < ActiveRecord::Migration[6.0]
  def change
    create_table :perms do |t|
      t.string :cargo
      t.string :comissaogerente
      t.string :comissaocorretor
      t.integer :idgerente
      t.references :user, null: false, foreign_key: true

      t.timestamps
    end
  end
end

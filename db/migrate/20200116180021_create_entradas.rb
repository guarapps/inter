class CreateEntradas < ActiveRecord::Migration[6.0]
  def change
    create_table :entradas do |t|
      t.references :banco, null: false, foreign_key: true
      t.date :dataentrada
      t.string :resumooperacao
      t.numeric :valor
      t.string :pago
      t.integer :idcorretor
      t.integer :idgerente
      t.references :cliente, null: false, foreign_key: true

      t.timestamps
    end
  end
end

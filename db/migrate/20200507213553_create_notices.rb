class CreateNotices < ActiveRecord::Migration[6.0]
  def change
    create_table :notices do |t|
      t.references :user, null: false, foreign_key: true
      t.integer :user_destino
      t.string :lido
      t.integer :grupo_destino
      t.text :mensagem
      t.integer :acao_id
      t.string :form

      t.timestamps
    end
  end
end

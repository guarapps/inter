class CreateDocumentos < ActiveRecord::Migration[6.0]
  def change
    create_table :documentos do |t|
      t.references :cliente, null: false, foreign_key: true
      t.string :nome_documento
      t.text :obs_documento

      t.timestamps
    end
  end
end

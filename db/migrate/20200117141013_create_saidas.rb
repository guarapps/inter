class CreateSaidas < ActiveRecord::Migration[6.0]
  def change
    create_table :saidas do |t|
      t.integer :banco_id
      t.date :datasaida
      t.string :resumooperacao
      t.numeric :valor
      t.string :pago
      t.integer :idcorretor
      t.integer :idgerente
      t.integer :cliente_id

      t.timestamps
    end
  end
end

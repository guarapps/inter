class CreateAtendimentos < ActiveRecord::Migration[6.0]
  def change
    create_table :atendimentos do |t|
      t.string :nome
      t.string :numero
      t.integer :corretorresponsavel

      t.timestamps
    end
  end
end

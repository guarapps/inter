class CreateClientes < ActiveRecord::Migration[6.0]
  def change
    create_table :clientes do |t|
      t.string :nome
      t.string :cpf
      t.string :tel
      t.string :matricula
      t.date :DataNascimento
      t.text :Obs

      t.timestamps
    end
  end
end

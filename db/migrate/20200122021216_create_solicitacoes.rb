class CreateSolicitacoes < ActiveRecord::Migration[6.0]
  def change
    create_table :solicitacoes do |t|
      t.string :titulo
      t.date :dataabertura
      t.string :finalizado
      t.references :user, null: false, foreign_key: true
      t.integer :userdestino

      t.timestamps
    end
  end
end

class CreateUsers < ActiveRecord::Migration[6.0]
  def change
    create_table :users do |t|
      t.string :nome
      t.string :cargo
      t.integer :comisgerente
      t.integer :comiscorretor
      t.text :obseravacao

      t.timestamps
    end
  end
end

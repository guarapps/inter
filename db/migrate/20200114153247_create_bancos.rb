class CreateBancos < ActiveRecord::Migration[6.0]
  def change
    create_table :bancos do |t|
      t.string :codigo
      t.string :nome
      t.text :obs

      t.timestamps
    end
  end
end

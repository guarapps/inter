class CreateStatusprotocolos < ActiveRecord::Migration[6.0]
  def change
    create_table :statusprotocolos do |t|
      t.references :protocolo, null: false, foreign_key: true
      t.string :status
      t.text :obs

      t.timestamps
    end
  end
end

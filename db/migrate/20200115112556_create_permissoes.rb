class CreatePermissoes < ActiveRecord::Migration[6.0]
  def change
    create_table :permissoes do |t|
      t.string :cargo
      t.integer :comisgerente
      t.integer :comiscorretor
      t.text :obseravacao
      t.references :user, null: false, foreign_key: true

      t.timestamps
    end
  end
end

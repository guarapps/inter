class AddTipoemprestimoToProtocolo < ActiveRecord::Migration[6.0]
  def change
    add_column :protocolos, :tipoemprestimo, :string
  end
end

class AddAtendimentoToCliente < ActiveRecord::Migration[6.0]
  def change
    add_reference :clientes, :atendimento, null: true, foreign_key: true
  end
end

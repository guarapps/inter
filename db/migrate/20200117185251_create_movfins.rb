class CreateMovfins < ActiveRecord::Migration[6.0]
  def change
    create_table :movfins do |t|
      t.integer :idmov
      t.string :tipo
      t.string :acao
      t.numeric :valor

      t.timestamps
    end
  end
end

class CreateLogs < ActiveRecord::Migration[6.0]
  def change
    create_table :logs do |t|
      t.references :user, null: false, foreign_key: true
      t.string :frase
      t.string :form
      t.string :idacao

      t.timestamps
    end
  end
end

# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_05_07_213553) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "active_storage_attachments", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.bigint "blob_id", null: false
    t.datetime "created_at", null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.bigint "byte_size", null: false
    t.string "checksum", null: false
    t.datetime "created_at", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "atendimentos", force: :cascade do |t|
    t.string "nome"
    t.string "numero"
    t.integer "corretorresponsavel"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "bancos", force: :cascade do |t|
    t.string "codigo"
    t.string "nome"
    t.text "obs"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "clientes", force: :cascade do |t|
    t.string "nome"
    t.string "cpf"
    t.string "tel"
    t.string "matricula"
    t.date "DataNascimento"
    t.text "Obs"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "senhaccheque"
    t.bigint "user_id"
    t.bigint "atendimento_id"
    t.index ["atendimento_id"], name: "index_clientes_on_atendimento_id"
    t.index ["user_id"], name: "index_clientes_on_user_id"
  end

  create_table "dados", force: :cascade do |t|
    t.string "cpf"
    t.string "telefone"
    t.string "conta"
    t.string "agencia"
    t.date "datainicio"
    t.date "datasaida"
    t.bigint "user_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "banco"
    t.index ["user_id"], name: "index_dados_on_user_id"
  end

  create_table "documentos", force: :cascade do |t|
    t.bigint "cliente_id", null: false
    t.string "nome_documento"
    t.text "obs_documento"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["cliente_id"], name: "index_documentos_on_cliente_id"
  end

  create_table "entradas", force: :cascade do |t|
    t.bigint "banco_id", null: false
    t.date "dataentrada"
    t.string "resumooperacao"
    t.decimal "valor"
    t.string "pago"
    t.integer "idcorretor"
    t.integer "idgerente"
    t.bigint "cliente_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["banco_id"], name: "index_entradas_on_banco_id"
    t.index ["cliente_id"], name: "index_entradas_on_cliente_id"
  end

  create_table "feedbacks", force: :cascade do |t|
    t.string "obs"
    t.bigint "user_id", null: false
    t.bigint "protocolo_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "cliente_id"
    t.index ["cliente_id"], name: "index_feedbacks_on_cliente_id"
    t.index ["protocolo_id"], name: "index_feedbacks_on_protocolo_id"
    t.index ["user_id"], name: "index_feedbacks_on_user_id"
  end

  create_table "feedbacktologs", force: :cascade do |t|
    t.text "obs"
    t.bigint "user_id", null: false
    t.bigint "log_id", null: false
    t.string "lido"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["log_id"], name: "index_feedbacktologs_on_log_id"
    t.index ["user_id"], name: "index_feedbacktologs_on_user_id"
  end

  create_table "itematendimentoclientes", force: :cascade do |t|
    t.string "numero"
    t.bigint "user_id", null: false
    t.date "quandoligou"
    t.date "quandoiraligar"
    t.string "numero1"
    t.string "numero2"
    t.text "obs"
    t.bigint "cliente_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["cliente_id"], name: "index_itematendimentoclientes_on_cliente_id"
    t.index ["user_id"], name: "index_itematendimentoclientes_on_user_id"
  end

  create_table "itematendimentos", force: :cascade do |t|
    t.bigint "atendimento_id", null: false
    t.string "quandoligou"
    t.string "quandoiraligar"
    t.string "numero1"
    t.string "numero2"
    t.text "obs"
    t.string "sehaprotocolo"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "user_id"
    t.index ["atendimento_id"], name: "index_itematendimentos_on_atendimento_id"
    t.index ["user_id"], name: "index_itematendimentos_on_user_id"
  end

  create_table "itemsolics", force: :cascade do |t|
    t.bigint "solicitacoe_id", null: false
    t.text "obs"
    t.bigint "user_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["solicitacoe_id"], name: "index_itemsolics_on_solicitacoe_id"
    t.index ["user_id"], name: "index_itemsolics_on_user_id"
  end

  create_table "logs", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.string "frase"
    t.string "form"
    t.string "idacao"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["user_id"], name: "index_logs_on_user_id"
  end

  create_table "movfins", force: :cascade do |t|
    t.integer "idmov"
    t.string "tipo"
    t.string "acao"
    t.decimal "valor"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "notices", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.integer "user_destino"
    t.string "lido"
    t.integer "grupo_destino"
    t.text "mensagem"
    t.integer "acao_id"
    t.string "form"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["user_id"], name: "index_notices_on_user_id"
  end

  create_table "permissoes", force: :cascade do |t|
    t.string "cargo"
    t.integer "comisgerente"
    t.integer "comiscorretor"
    t.text "obseravacao"
    t.bigint "user_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["user_id"], name: "index_permissoes_on_user_id"
  end

  create_table "perms", force: :cascade do |t|
    t.string "cargo"
    t.string "comissaogerente"
    t.string "comissaocorretor"
    t.integer "idgerente"
    t.bigint "user_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["user_id"], name: "index_perms_on_user_id"
  end

  create_table "posts", force: :cascade do |t|
    t.string "titulo"
    t.text "text"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "protocolos", force: :cascade do |t|
    t.decimal "valorparcela"
    t.integer "prazo"
    t.decimal "valorcontrato"
    t.decimal "prestservico"
    t.bigint "cliente_id", null: false
    t.bigint "banco_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "idcorretor"
    t.string "tipoemprestimo"
    t.string "orgao"
    t.index ["banco_id"], name: "index_protocolos_on_banco_id"
    t.index ["cliente_id"], name: "index_protocolos_on_cliente_id"
  end

  create_table "saidas", force: :cascade do |t|
    t.integer "banco_id"
    t.date "datasaida"
    t.string "resumooperacao"
    t.decimal "valor"
    t.string "pago"
    t.integer "idcorretor"
    t.integer "idgerente"
    t.integer "cliente_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "solicitacoes", force: :cascade do |t|
    t.string "titulo"
    t.date "dataabertura"
    t.string "finalizado"
    t.bigint "user_id", null: false
    t.integer "userdestino"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["user_id"], name: "index_solicitacoes_on_user_id"
  end

  create_table "statusprotocolos", force: :cascade do |t|
    t.bigint "protocolo_id", null: false
    t.string "status"
    t.text "obs"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "progress"
    t.index ["protocolo_id"], name: "index_statusprotocolos_on_protocolo_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "nome"
    t.string "cargo"
    t.integer "comisgerente"
    t.integer "comiscorretor"
    t.text "obseravacao"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  add_foreign_key "active_storage_attachments", "active_storage_blobs", column: "blob_id"
  add_foreign_key "clientes", "atendimentos"
  add_foreign_key "clientes", "users"
  add_foreign_key "dados", "users"
  add_foreign_key "documentos", "clientes"
  add_foreign_key "entradas", "bancos"
  add_foreign_key "entradas", "clientes"
  add_foreign_key "feedbacks", "clientes"
  add_foreign_key "feedbacks", "protocolos"
  add_foreign_key "feedbacks", "users"
  add_foreign_key "feedbacktologs", "logs"
  add_foreign_key "feedbacktologs", "users"
  add_foreign_key "itematendimentoclientes", "clientes"
  add_foreign_key "itematendimentoclientes", "users"
  add_foreign_key "itematendimentos", "atendimentos"
  add_foreign_key "itematendimentos", "users"
  add_foreign_key "itemsolics", "solicitacoes", column: "solicitacoe_id"
  add_foreign_key "itemsolics", "users"
  add_foreign_key "logs", "users"
  add_foreign_key "notices", "users"
  add_foreign_key "permissoes", "users"
  add_foreign_key "perms", "users"
  add_foreign_key "protocolos", "bancos"
  add_foreign_key "protocolos", "clientes"
  add_foreign_key "solicitacoes", "users"
  add_foreign_key "statusprotocolos", "protocolos"
end

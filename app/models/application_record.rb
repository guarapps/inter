class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true
  require 'brazilian-rails'


  def self.comissao_paga(id)
    Saida.where("updated_at between ? and ? and pago = 'Pago' and  idcorretor = ? ",
                "2020-#{DateTime.now.to_s[5..6]}-01",
                "#{Date.today.end_of_month}","#{id}")
          .sum("valor")
  end
end

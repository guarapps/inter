class Cliente < ApplicationRecord
  
  has_many :protocolos
  has_many :itematendimentoclientes
  has_many :entradas
  has_many :feedbacks
  belongs_to :atendimento, optional: true
  has_many :documentos
  validates  :nome , :presence => {:message => ": é obrigatório !"}
  validates  :cpf , :presence => {:message => ": é obrigatório !"}

  def cpfcliente
    "#{self.cpf.to_s[0..8]}-#{self.cpf.to_s[9..10]}"
  end

  def telcliente
    "(#{self.tel.to_s[0..2]})#{self.tel.to_s[3..7]}-#{self.tel.to_s[8..11]}"
  end

  def datanacimentocliente
    "#{self.DataNascimento.to_s[8..9]}/#{self.DataNascimento.to_s[5..6]}/#{self.DataNascimento.to_s[0..3]}"
  end

  def self.formatdata(texto)
      "#{texto.to_s[8..9]}/#{texto.to_s[5..6]}/#{texto.to_s[0..3]}"
  end
end

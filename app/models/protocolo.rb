class Protocolo < ApplicationRecord
  belongs_to :cliente
  belongs_to :banco
  has_many :statusprotocolos
  has_many :feedbacks
end

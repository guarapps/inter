class Itematendimentocliente < ApplicationRecord
  belongs_to :user
  belongs_to :cliente
  def self.formatdata(texto)
      "#{texto.to_s[8..9]}/#{texto.to_s[5..6]}/#{texto.to_s[0..3]}"
  end
end

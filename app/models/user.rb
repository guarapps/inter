class User < ApplicationRecord
  # cargo: [:admin, :operacional, :gerente,:corretor]
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable
  def set_default_role
    self.role ||= :corretor
  end
  has_many :dados
  has_many :perms
end

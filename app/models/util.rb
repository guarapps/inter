# frozen_string_literal: true

class Util

  # :need-doc:
  def self.converter_data_ano_mes(data)
    if data
      ano_mes = data.year.to_s + data.strftime('%m').to_s.rjust(2, '0')
      return ano_mes.to_i
    end
  end

  # :need-doc:
  def self.converter_data_mes_ano_str(data)
    if data
      ano_mes = data.strftime('%m').to_s.rjust(2, '0') + '/' + data.year.to_s
      return ano_mes.to_s
    end
  end


  #converte a data em referencia ano/mes
  def self.convert_data_referencia_not_null(data)
    data ||= DateTime.now
    Util.converter_data_ano_mes(data.to_date)
  end

  # :need-doc:
  def self.proximo_dia_util(dta_verificacao = DateTime.now)
    data = dta_verificacao.to_date
    while true
      data = data.to_date + 1.day
      unless RecursosHumanos::TbFeriado.tem_feriado?(data)
        break
      end
    end
    data
  end

  # Mêses entre duas datas
  def self.months_between(datefim, dateini)
    months_fim = datefim.year * 12 + datefim.month
    months_ini = dateini.year * 12 + dateini.month
    months_fim - months_ini
  end

  # :need-doc:
  def self.formata_moeda_com_cifrao(valor)
    if valor && (valor > 0)
      Dinheiro.new(valor.to_s.to_f).real_formatado
    else
      Dinheiro.new(0).real_formatado
    end
  end

  # :need-doc:
  def self.monta_ref_anomes(ano, mes)
    ref_anomes = nil
    if ano && mes
      ref_anomes = (ano.to_s.ljust(4, '0').to_s + mes.to_s.rjust(2, '0')).to_i
    end
    ref_anomes
  end

  # extension filename
  def self.extension_filename(file_name)
    if file_name && File.extname(file_name) && File.extname(file_name).present?
      File.extname(file_name).strip.downcase[1..-1]
    else
      'PDF'
    end
  end


  # :need-doc:
  def self.format_date_to_str_with_sql(data)
    if data && data.present?
      "TO_DATE('#{data.to_s_br}','DD/MM/YYYY')"
    else
      ''
    end
  end

  # :need-doc:
  def self.formata_moeda_sem_ponto(valor)
    if valor && (valor > 0)
      Dinheiro.new(valor)
    else
      '0,00'
    end
  end


  # Retorna a idade
  def self.age(data)
    now = Date.today
    age = now.year - data.year
    age -= 1 if data > now.years_ago(age)
    age
  end

  # Retorna a diferença entre 2 times
  def self.time_diff(start_time, end_time)
    seconds_diff = (start_time - end_time).to_i.abs

    hours = seconds_diff / 3600
    seconds_diff -= hours * 3600

    minutes = seconds_diff / 60
    seconds_diff -= minutes * 60

    seconds = seconds_diff

    "#{hours.to_s.rjust(2, '0')}:#{minutes.to_s.rjust(2, '0')}:#{seconds.to_s.rjust(2, '0')}"
    # or, as hagello suggested in the comments:
    # '%02d:%02d:%02d' % [hours, minutes, seconds]
  end


  # :need-doc:
  def self.downcase_hash_keys_list(lista)
    lista.each { |e| e = downcase_hash_keys(e).symbolize_keys }
    lista.each do |h|
      h.keys.each { |k| h[k.to_sym] = h.delete(k) }
    end
  end

  # :need-doc:
  def self.downcase_hash_keys(h)
    if h.is_a?(Hash)
      h.keys.each do |key|
        new_key = key.to_s.downcase
        h[new_key] = h.delete(key)
        downcase_hash_keys(h[new_key])
      end
    elsif h.respond_to?(:each)
      h.each { |e| downcase_hash_keys(e) }
    end
    h
  end

  # :need-doc:
  def self.array_to_hash(lista)
    hash = Hash.new
    lista.each { |h| hash[h[0].to_sym] = h[1] }
    hash
  end

  # :need-doc:
  def self.lowercase_keys(h)
    new_h = {}
    h.each_pair do |k, v|
      new_k = deep_lowercase(k, true)
      new_v = deep_lowercase(v, false)
      new_h[new_k] = new_v
    end
    new_h
  end

  # :need-doc:
  def self.deep_lowercase(object, with_strings)
    case object
    when String then
      with_strings ? object.downcase : object
    when Hash then
      lowercase_keys(object)
    else
      object.respond_to?(:map) ? object.map { |e| deep_lowercase(e, false) } : object
    end
  end
end

class Banco < ApplicationRecord
  validates  :codigo , :presence => {:message => ": O Código do Banco é obrigatório !"}
  validates  :nome , :presence => {:message => ": O Nome do Banco é obrigatório !"}
end

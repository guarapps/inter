class Saida < ApplicationRecord
  belongs_to :banco
  has_one_attached :comprovante
  usar_como_dinheiro :valor

  after_update do
    case  self.pago
    when 'Pago'
    movfin = Movfin.new
    movfin.idmov= self.id
    movfin.tipo= 'Saida'
    movfin.acao= "Pagamento"
    movfin.valor= self.valor
    movfin.save
    when 'Extorno'
    movfin = Movfin.new
    movfin.idmov= self.id
    movfin.tipo= 'Saida'
    movfin.acao= "Extorno de Pagamento"
    movfin.valor= self.valor*-1
    movfin.save
    end
  end
end

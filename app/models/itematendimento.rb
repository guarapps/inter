class Itematendimento < ApplicationRecord
  belongs_to :atendimento

  after_create do
    user = user_id
      self.atendimento.update(corretorresponsavel: user)
  end
  def self.formatdata(texto)
      "#{texto.to_s[8..9]}/#{texto.to_s[5..6]}/#{texto.to_s[0..3]}"
  end
end

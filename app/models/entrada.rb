class Entrada < ApplicationRecord
  paginates_per 10
  has_one_attached :comprovante

  after_update do
    case  self.pago
    when 'Pago'
    movfin = Movfin.new
    movfin.idmov= self.id
    movfin.tipo= 'Entrada'
    movfin.acao= "Recebimento"
    movfin.valor= self.valor
    movfin.save
    when 'Extorno'
    movfin = Movfin.new
    movfin.idmov= self.id
    movfin.tipo= 'Entrada'
    movfin.acao= "Extorno"
    movfin.valor= self.valor*-1
    movfin.save
    end
  end
end

class Statusprotocolo < ApplicationRecord
  belongs_to :protocolo
  has_many :user

  #Gerando Entrada
  after_update do
    if self.progress = "100"
      corretor = Protocolo.where("id = ?","#{Statusprotocolo.where("id = ?","#{self.id}").pluck("protocolo_id").first}").pluck("idcorretor").first
      gerente = Perm.where("user_id = ?","#{Protocolo.where("id = ?","#{Statusprotocolo.where("id = ?","#{self.id}").pluck("protocolo_id").first}").pluck("idcorretor").first}").pluck("idgerente").first
      banco = Protocolo.where("id = ?","#{Statusprotocolo.where("id = ?","#{self.id}").pluck("protocolo_id").first}").pluck("banco_id").first
      cliente =  Protocolo.where("id = ?","#{Statusprotocolo.where("id = ?","#{self.id}").pluck("protocolo_id").first}").pluck("cliente_id").first
      valorservico = Protocolo.where("id = ?","#{Statusprotocolo.where("id = ?","#{self.id}").pluck("protocolo_id").first}").pluck("prestservico").first.to_f

      entrada = Entrada.new
      entrada.banco_id = banco
      entrada.dataentrada = DateTime.now.to_date
      entrada.resumooperacao = "Entrada gerada automaticamente pela conclusão do contrato do cliente: #{Cliente.where("id = ?","#{cliente}").pluck("nome").first.titleize} "
      entrada.valor = valorservico
      entrada.pago = "Em aberto"
      entrada.idcorretor = corretor
      entrada.idgerente = gerente
      entrada.cliente_id = cliente
      entrada.save
    end
  end

  #Gerando comissão
  after_update do
    if self.progress = "100"

      cargo = Perm.where("user_id = ?","#{Protocolo.where("id = ?","#{Statusprotocolo.where("id = ?","#{self.id}").pluck("protocolo_id").first}").pluck("idcorretor").first}").pluck("cargo").first

      case  cargo
      when '2'
        #Gerente
        idgerente = Protocolo.where("id = ?","#{Statusprotocolo.where("id = ?","#{self.id}").pluck("protocolo_id").first}").pluck("idcorretor").first
        banco = Protocolo.where("id = ?","#{Statusprotocolo.where("id = ?","#{self.id}").pluck("protocolo_id").first}").pluck("banco_id").first
        cliente =  Protocolo.where("id = ?","#{Statusprotocolo.where("id = ?","#{self.id}").pluck("protocolo_id").first}").pluck("cliente_id").first
        valorservico = Protocolo.where("id = ?","#{Statusprotocolo.where("id = ?","#{self.id}").pluck("protocolo_id").first}").pluck("prestservico").first.to_f
        comissaocorretor = Perm.where("user_id = ?","#{Protocolo.where("id = ?","#{Statusprotocolo.where("id = ?","#{self.id}").pluck("protocolo_id").first}").pluck("idcorretor").first}").pluck("comissaocorretor").first
        corretor = Protocolo.where("id = ?","#{Statusprotocolo.where("id = ?","#{self.id}").pluck("protocolo_id").first}").pluck("idcorretor").first
        gerente = Perm.where("user_id = ?","#{Protocolo.where("id = ?","#{Statusprotocolo.where("id = ?","#{self.id}").pluck("protocolo_id").first}").pluck("idcorretor").first}").pluck("idgerente").first
        idcorretorcomissao = Perm.where("user_id = ?","#{corretor}").pluck("comissaogerente").first
        cem = 100.to_f
        valor =  (valorservico * (comissaocorretor.to_f/100)).to_f

        #criacao da saida na tabela
        saida = Saida.new
        saida.banco_id = banco
        saida.cliente_id = cliente
        saida.datasaida = DateTime.now.to_date
        saida.resumooperacao = "Comissão automatica gerada pela conclusão de contrato do cliente: #{Cliente.where("id = ?","#{cliente}").pluck("nome").first.titleize}"
        saida.valor = valor
        saida.pago = "Em aberto"
        saida.idgerente = corretor
        saida.save
      when '3'
        #Corretor e Gerente
        corretor = Protocolo.where("id = ?","#{Statusprotocolo.where("id = ?","#{self.id}").pluck("protocolo_id").first}").pluck("idcorretor").first
        gerente = Perm.where("user_id = ?","#{Protocolo.where("id = ?","#{Statusprotocolo.where("id = ?","#{self.id}").pluck("protocolo_id").first}").pluck("idcorretor").first}").pluck("idgerente").first

        cocorretor = Perm.where("user_id = ?","#{corretor}").pluck("comissaocorretor").first
        cogerente = Perm.where("user_id = ?","#{gerente}").pluck("comissaogerente").first


        banco = Protocolo.where("id = ?","#{Statusprotocolo.where("id = ?","#{self.id}").pluck("protocolo_id").first}").pluck("banco_id").first
        cliente =  Protocolo.where("id = ?","#{Statusprotocolo.where("id = ?","#{self.id}").pluck("protocolo_id").first}").pluck("cliente_id").first

        valorservico = Protocolo.where("id = ?","#{Statusprotocolo.where("id = ?","#{self.id}").pluck("protocolo_id").first}").pluck("prestservico").first.to_f

        vlrcocorretor = (valorservico * (cocorretor.to_f/100)).to_f
        vlrcogerente = (valorservico * (cogerente.to_f/100)).to_f


        saida = Saida.new
        saida.banco_id = banco
        saida.cliente_id = cliente
        saida.datasaida = DateTime.now.to_date
        saida.resumooperacao = "Comissão automatica gerada pela conclusão de contrato do cliente: #{Cliente.where("id = ?","#{cliente}").pluck("nome").first}"
        saida.valor = vlrcocorretor
        saida.pago = "Em aberto"
        saida.idcorretor = corretor
        saida.save

        saida = Saida.new
        saida.banco_id = banco
        saida.cliente_id = cliente
        saida.datasaida = DateTime.now.to_date
        saida.resumooperacao = "Comissão automatica gerada pela conclusão de contrato do cliente: #{Cliente.where("id = ?","#{cliente}").pluck("nome").first}"
        saida.valor = vlrcogerente
        saida.pago = "Em aberto"
        saida.idgerente = gerente
        saida.save
      end
    else
    end
  end
end

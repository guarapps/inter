json.extract! saida, :id, :banco_id, :datasaida, :resumooperacao, :valor, :pago, :idcorretor, :idgerente, :cliente_id, :created_at, :updated_at
json.url saida_url(saida, format: :json)

json.extract! protocolo, :id, :valorparcela, :prazo, :valorcontrato, :prestservico, :cliente_id, :banco_id, :created_at, :updated_at
json.url protocolo_url(protocolo, format: :json)

json.extract! notice, :id, :user_id, :user_destino, :lido, :grupo_destino, :mensagem, :acao_id, :form, :created_at, :updated_at
json.url notice_url(notice, format: :json)

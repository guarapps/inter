json.extract! permisso, :id, :cargo, :comisgerente, :comiscorretor, :obseravacao, :user_id, :created_at, :updated_at
json.url permisso_url(permisso, format: :json)

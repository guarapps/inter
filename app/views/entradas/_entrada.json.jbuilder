json.extract! entrada, :id, :banco_id, :dataentrada, :resumooperacao, :valor, :pago, :idcorretor, :idgerente, :cliente_id, :created_at, :updated_at
json.url entrada_url(entrada, format: :json)

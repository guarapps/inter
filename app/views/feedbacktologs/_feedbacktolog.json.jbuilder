json.extract! feedbacktolog, :id, :obs, :user_id, :log_id, :lido, :created_at, :updated_at
json.url feedbacktolog_url(feedbacktolog, format: :json)

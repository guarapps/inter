json.extract! documento, :id, :cliente_id, :nome_documento, :obs_documento, :created_at, :updated_at
json.url documento_url(documento, format: :json)

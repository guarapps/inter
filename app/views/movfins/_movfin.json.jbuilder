json.extract! movfin, :id, :idmov, :tipo, :acao, :valor, :created_at, :updated_at
json.url movfin_url(movfin, format: :json)

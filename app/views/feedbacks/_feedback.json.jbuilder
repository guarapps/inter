json.extract! feedback, :id, :obs, :user_id, :protocolo_id, :created_at, :updated_at
json.url feedback_url(feedback, format: :json)

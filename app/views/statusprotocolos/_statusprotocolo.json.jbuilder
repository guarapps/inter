json.extract! statusprotocolo, :id, :protocolo_id, :status, :obs, :created_at, :updated_at
json.url statusprotocolo_url(statusprotocolo, format: :json)

module ApplicationHelper
 # Define um input para dinheiro
  def input_dinheiro_precisao(form, field, label, options = {})
    value = Util.formata_moeda_sem_ponto(form.object.attributes[field.to_s]) if form.object.attributes[field.to_s]
    value = Util.formata_moeda_sem_ponto(options[:value].to_f) if options[:value].present?
    "#{form.text_field field,
                       value: (value),
                       prepend: (options[:prepend].present? ? options[:prepend] : 'R$'), label: label, name: "#{options[:id]}_mask_money", "id" => "#{options[:id]}_mask_money",
                       placeholder: options[:place_holder], data_required: options[:required],
                       hide_label: label.nil?,
                       onchange: options[:onchange].to_s,
                       class: "form-control input-small input-money #{options[:classe] || 'input'}",
                       "data-decimal" => options[:separador_centavos] || ',',
                       "data-thousands" => options[:separador_centena] || '.',
                       onkeyup: "$(\"[name='#{form.object_name}[#{field}]']\").val(this.value.replace(/\\./g, '').replace(/\\,/g,'.'));"}
    #{form.hidden_field field}".html_safe
  end


  def diferenca(date_origem)
    if date_origem.hour == DateTime.now.hour
      minuto = "#{((date_origem.min - DateTime.now.min)*-1)} minutos"
    elsif (date_origem.hour - DateTime.now.hour) == (-1)   
      minuto = "#{(((date_origem.min - 60) + DateTime.now.min))} minutos"
    else
      minuto = "1+ hora"
    end
      minuto 
  end

  def primeiro_nome(id)
    nome = User.where(id:id).pluck(:nome).first
    nome_split = nome.split
    primeiro_nome = nome_split[0]  
    
    primeiro_nome
  end
end

class ItematendimentosController < ApplicationController
  before_action :set_itematendimento, only: [:show, :edit, :update, :destroy]

  # GET /itematendimentos
  # GET /itematendimentos.json
  def index
    @itematendimentos = Itematendimento.all
  end

  # GET /itematendimentos/1
  # GET /itematendimentos/1.json
  def show
  end

  # GET /itematendimentos/new
  def new
    @itematendimento = Itematendimento.new
  end

  # GET /itematendimentos/1/edit
  def edit
  end

  # POST /itematendimentos
  # POST /itematendimentos.json
  def create
    @itematendimento = Itematendimento.new(itematendimento_params)
    

    respond_to do |format|
      if @itematendimento.save
        format.html { redirect_to controller:"atendimentos",action:"show",id:@itematendimento.atendimento_id, notice: 'Itematendimento was successfully created.' }
        format.json { render :show, status: :created, location: @itematendimento }
      else
        format.html { render :new }
        format.json { render json: @itematendimento.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /itematendimentos/1
  # PATCH/PUT /itematendimentos/1.json
  def update
    respond_to do |format|
      if @itematendimento.update(itematendimento_params)
        format.html { redirect_to controller:"atendimentos",action:"show",id:@itematendimento.atendimento_id, notice: 'Itematendimento was successfully updated.' }
        format.json { render :show, status: :ok, location: @itematendimento }
      else
        format.html { render :edit }
        format.json { render json: @itematendimento.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /itematendimentos/1
  # DELETE /itematendimentos/1.json
  def destroy
    @itematendimento.destroy
    respond_to do |format|
      format.html { redirect_to itematendimentos_url, notice: 'Itematendimento was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_itematendimento
      @itematendimento = Itematendimento.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def itematendimento_params
      params.require(:itematendimento).permit(:atendimento_id, :quandoligou, :quandoiraligar, :numero1, :numero2, :obs, :sehaprotocolo,:user_id)
    end
end

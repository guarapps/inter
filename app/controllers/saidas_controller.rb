class SaidasController < ApplicationController
  before_action :set_saida, only: [:show, :edit, :update, :destroy]

  # GET /saidas
  # GET /saidas.json
  def index
    if params[:datainisearch] and params[:datafinsearch]
      @saidas = Saida.where("datasaida between ? and ?","#{params[:datainisearch]}","#{params[:datafinsearch]}")
      @totalpago = Saida.where("datasaida between ? and ? and pago = 'Pago'","#{params[:datainisearch]}","#{params[:datafinsearch]}").sum("valor")
      @totalinativo = Saida.where("datasaida between ? and ?  and pago = 'Inativo'","#{params[:datainisearch]}","#{params[:datafinsearch]}").sum("valor")
      @totalextorno = Saida.where("datasaida between ? and ?  and pago = 'Extorno'","#{params[:datainisearch]}","#{params[:datafinsearch]}").sum("valor")
    else
      @saidas = Saida.all
      @totalpago = Saida.where("pago = 'Pago'").sum("valor")
      @totalinativo = Saida.where("pago = 'Inativo'").sum("valor")
      @totalextorno = Saida.where("pago = 'Extorno'").sum("valor")
    end

    if params[:datainipdf] and params[:datafinpdf] and params[:gerar_pdf]
      @saidas = Saida.where("datasaida between ? and ?","#{params[:datainipdf]}","#{params[:datafinpdf]}")
      @totalpago = Saida.where("datasaida between ? and ? and pago = 'Pago'","#{params[:datainipdf]}","#{params[:datafinpdf]}").sum("valor")
      @totalinativo = Saida.where("datasaida between ? and ?  and pago = 'Inativo'","#{params[:datainipdf]}","#{params[:datafinpdf]}").sum("valor")
      @totalextorno = Saida.where("datasaida between ? and ?  and pago = 'Extorno'","#{params[:datainipdf]}","#{params[:datafinpdf]}").sum("valor")
      render pdf:"Saidas",
      page_size: 'A4',
      template: "saidas/index.pdf.erb",
      layout:"pdf.html",
      lowquality: true,
      zoon: 1,
      dpi: 75
    end
    @users = User.all
  end

  # GET /saidas/1
  # GET /saidas/1.json
  def show
  end

  # GET /saidas/new
  def new
    @clientes = Cliente.all
    @users = User.all
    @bancos = Banco.all
    @saida = Saida.new
  end

  # GET /saidas/1/edit
  def edit
     @bancos = Banco.all
     @users= User.all
  end

  # POST /saidas
  # POST /saidas.json
  def create
    @clientes = Cliente.all
    @users = User.all
    @bancos = Banco.all
    @saida = Saida.new(saida_params)
    Log.create(frase: 'Lançou uma Saida',user_id: current_user.id,form: 'SAIDA',idacao: "#{Saida.all.pluck("max(id)").first + 1}")
    respond_to do |format|
      if @saida.save
        format.html { redirect_to @saida, notice: 'Saida was successfully created.' }
        format.json { render :show, status: :created, location: @saida }
      else
        format.html { render :new }
        format.json { render json: @saida.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /saidas/1
  # PATCH/PUT /saidas/1.json
  def update
    Log.create(frase: 'Alterou uma Saida',user_id: current_user.id,form: 'SAIDA',idacao: @saida.id)
    respond_to do |format|
      if @saida.update(saida_params)
        format.html { redirect_to @saida, notice: 'Saida was successfully updated.' }
        format.json { render :show, status: :ok, location: @saida }
      else
        format.html { render :edit }
        format.json { render json: @saida.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /saidas/1
  # DELETE /saidas/1.json
  def destroy
    @saida.destroy
    respond_to do |format|
      format.html { redirect_to saidas_url, notice: 'Saida was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_saida
      @saida = Saida.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def saida_params
      params.require(:saida).permit(:banco_id, :datasaida, :resumooperacao, :valor, :pago, :idcorretor, :idgerente, :cliente_id,:comprovante)
    end
end

class StatusprotocolosController < ApplicationController
  before_action :set_statusprotocolo, only: [:show, :edit, :update, :destroy]

  # GET /statusprotocolos
  # GET /statusprotocolos.json
  def index
    @statusprotocolos = Statusprotocolo.all
  end

  # GET /statusprotocolos/1
  # GET /statusprotocolos/1.json
  def show
  end

  # GET /statusprotocolos/new
  def new
    @statusprotocolo = Statusprotocolo.new
  end

  # GET /statusprotocolos/1/edit
  def edit
  end

  # POST /statusprotocolos
  # POST /statusprotocolos.json
  def create
    Log.create(frase: 'Lançou um Status de um Protocolo',user_id: current_user.id,form: 'PROTOCOLO',idacao: params[:protocolo_id])
    @statusprotocolo = Statusprotocolo.new(statusprotocolo_params)
    respond_to do |format|
      if @statusprotocolo.save
        format.html { redirect_to controller:'protocolos',action:'show', id:@statusprotocolo.protocolo_id, notice: 'Statusprotocolo was successfully created.' }
        format.json { render :show, status: :created, location: @statusprotocolo }
      else
        format.html { render :new }
        format.json { render json: @statusprotocolo.errors, status: :unprocessable_entity }
      end
    end
  end


  # PATCH/PUT /statusprotocolos/1
  # PATCH/PUT /statusprotocolos/1.json
  def update
    Log.create(frase: 'Mudou Status de um Protocolo',user_id: current_user.id,form: 'PROTOCOLO',idacao: @statusprotocolo.protocolo_id)
    respond_to do |format|
      if @statusprotocolo.update(statusprotocolo_params)
        format.html { redirect_to controller:'protocolos',action:'show', id:@statusprotocolo.protocolo_id, notice: 'Statusprotocolo was successfully updated.' }
        format.json { render :show, status: :ok, location: @statusprotocolo }
      else
        format.html { render :edit }
        format.json { render json: @statusprotocolo.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /statusprotocolos/1
  # DELETE /statusprotocolos/1.json
  def destroy
    @statusprotocolo.destroy
    respond_to do |format|
      format.html { redirect_to statusprotocolos_url, notice: 'Statusprotocolo was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_statusprotocolo
      @statusprotocolo = Statusprotocolo.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def statusprotocolo_params
      params.require(:statusprotocolo).permit(:protocolo_id, :status, :obs,:progress,:user_id)
    end
end

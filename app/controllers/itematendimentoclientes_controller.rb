class ItematendimentoclientesController < ApplicationController
  before_action :set_itematendimentocliente, only: [:show, :edit, :update, :destroy]

  # GET /itematendimentoclientes
  # GET /itematendimentoclientes.json
  def index
    @itematendimentoclientes = Itematendimentocliente.all
  end

  # GET /itematendimentoclientes/1
  # GET /itematendimentoclientes/1.json
  def show
  end

  # GET /itematendimentoclientes/new
  def new
    @itematendimentocliente = Itematendimentocliente.new
  end

  # GET /itematendimentoclientes/1/edit
  def edit
  end

  # POST /itematendimentoclientes
  # POST /itematendimentoclientes.json
  def create
    @itematendimentocliente = Itematendimentocliente.new(itematendimentocliente_params)
    Log.create(frase: 'Atualizou um Atedimento',user_id: current_user.id,form: 'ATEDIMENTO CLIENTE',idacao: params[:cliente_id])
    respond_to do |format|
      if @itematendimentocliente.save
        format.html { redirect_to controller:"clientes",action:"show",id:@itematendimentocliente.cliente_id, notice: 'Itematendimentocliente was successfully created.' }
        format.json { render :show, status: :created, location: @itematendimentocliente }
      else
        format.html { render :new }
        format.json { render json: @itematendimentocliente.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /itematendimentoclientes/1
  # PATCH/PUT /itematendimentoclientes/1.json
  def update
    respond_to do |format|
      if @itematendimentocliente.update(itematendimentocliente_params)
        format.html { redirect_to  controller:"clientes",action:"show",id:@itematendimentocliente.cliente_id, notice: 'Itematendimentocliente was successfully updated.' }
        format.json { render :show, status: :ok, location: @itematendimentocliente }
      else
        format.html { render :edit }
        format.json { render json: @itematendimentocliente.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /itematendimentoclientes/1
  # DELETE /itematendimentoclientes/1.json
  def destroy
    @itematendimentocliente.destroy
    respond_to do |format|
      format.html { redirect_to itematendimentoclientes_url, notice: 'Itematendimentocliente was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_itematendimentocliente
      @itematendimentocliente = Itematendimentocliente.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def itematendimentocliente_params
      params.require(:itematendimentocliente).permit(:numero, :user_id, :quandoligou, :quandoiraligar, :numero1, :numero2, :obs, :cliente_id)
    end
end

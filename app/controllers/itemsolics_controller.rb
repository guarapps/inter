class ItemsolicsController < ApplicationController
  before_action :set_itemsolic, only: [:show, :edit, :update, :destroy]

  # GET /itemsolics
  # GET /itemsolics.json
  def index
    @itemsolics = Itemsolic.all
  end

  # GET /itemsolics/1
  # GET /itemsolics/1.json
  def show
  end

  # GET /itemsolics/new
  def new
    @itemsolic = Itemsolic.new
  end

  # GET /itemsolics/1/edit
  def edit
  end

  # POST /itemsolics
  # POST /itemsolics.json
  def create
    @itemsolic = Itemsolic.new(itemsolic_params)
    Log.create(frase: 'Respondeu uma Solicitação',user_id: current_user.id,form: 'SOLICITAÇÃO',idacao: @itemsolic.solicitacoe_id)
    respond_to do |format|
      if @itemsolic.save
        #controller:"solicitacoes",action:"show",id:@itemsolic.solicitacoe_id
        format.html { redirect_to controller:"solicitacoes",action:"show",id:@itemsolic.solicitacoe_id}
        format.json { render :show, status: :created, location: @itemsolic }
      else
        format.html { render :new }
        format.json { render json: @itemsolic.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /itemsolics/1
  # PATCH/PUT /itemsolics/1.json
  def update
    respond_to do |format|
      if @itemsolic.update(itemsolic_params)
        format.html { redirect_to @itemsolic, notice: 'Itemsolic was successfully updated.' }
        format.json { render :show, status: :ok, location: @itemsolic }
      else
        format.html { render :edit }
        format.json { render json: @itemsolic.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /itemsolics/1
  # DELETE /itemsolics/1.json
  def destroy
    @itemsolic.destroy
    respond_to do |format|
      format.html { redirect_to itemsolics_url, notice: 'Itemsolic was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_itemsolic
      @itemsolic = Itemsolic.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def itemsolic_params
      params.require(:itemsolic).permit(:solicitacoe_id, :obs, :user_id)
    end
end

class PaginainicialController < ApplicationController

  def index
    @posttittle = Post.pluck("titulo").last
    @post = Post.pluck("text").last

    require 'brazilian-rails'

    mês = DateTime.now.to_s[5..6]
    ultimodia = case mês
    when '01'
      '31'
    when '02'
      '28'
    when '03'
      '31'
    when '04'
      '30'
    when '05'
      '31'
    when '06'
      '30'
    when '07'
      '31'
    when '08'
      '31'
    when '09'
      '30'
    when '10'
      '31'
    when '11'
      '30'
    when '12'
    '31'
    end

    mêspassado = (DateTime.now - 1.month).to_s[5..6]
    ultimodiamês = case mêspassado
    when '01'
      '31'
    when '02'
      '28'
    when '03'
      '31'
    when '04'
      '30'
    when '05'
      '31'
    when '06'
      '30'
    when '07'
      '31'
    when '08'
      '31'
    when '09'
      '30'
    when '10'
      '31'
    when '11'
      '30'
    when '12'
    '31'
    end

    #Corretor/Gerente#
    @comissaogerentepago = Saida.where("updated_at between ? and ? and pago = 'Pago' and  idgerente = ? ","2020-#{DateTime.now.to_s[5..6]}-01","2020-#{DateTime.now.to_s[5..6]}-#{ultimodia}","#{current_user.id}").sum("valor")
    @comissaogerenteemaberto = Saida.where("updated_at between ? and ? and pago = 'Em aberto' and idgerente = ? ","2020-#{DateTime.now.to_s[5..6]}-01","2020-#{DateTime.now.to_s[5..6]}-#{ultimodia}","#{current_user.id}").sum("valor")
    @comissaogerentemespassado =  Saida.where("updated_at between ? and ? and pago = 'Pago' and idgerente = ? ","2020-#{(DateTime.now - 1.month).to_s[5..6]}-01","2020-#{(DateTime.now - 1.month).to_s[5..6]}-#{ultimodiamês}","#{current_user.id}").sum("valor")


    @comissaocorretorpago = Saida.where("updated_at between ? and ? and pago = 'Pago' and  idcorretor = ? ","2020-#{DateTime.now.to_s[5..6]}-01","2020-#{DateTime.now.to_s[5..6]}-#{ultimodia}","#{current_user.id}").sum("valor")
    @comissaocorretoremaberto = Saida.where("updated_at between ? and ? and pago = 'Em aberto' and idcorretor = ? ","2020-#{DateTime.now.to_s[5..6]}-01","2020-#{DateTime.now.to_s[5..6]}-#{ultimodia}","#{current_user.id}").sum("valor")
    @comissaocorretormespassado =  Saida.where("updated_at between ? and ? and pago = 'Pago' and idcorretor = ? ","2020-#{(DateTime.now - 1.month).to_s[5..6]}-01","2020-#{(DateTime.now - 1.month).to_s[5..6]}-#{ultimodiamês}","#{current_user.id}").sum("valor")
    @retornoshoje = Atendimento.joins("join itematendimentos on itematendimentos.atendimento_id = atendimentos.id").where("user_id = ? and itematendimentos.quandoiraligar = ?","#{current_user.id}","#{DateTime.now.to_s[0..9]}").count
    #Amanhafuncition
    amanhã = DateTime.now + 1.day
    @retornosamanha = Atendimento.joins("join itematendimentos on itematendimentos.atendimento_id = atendimentos.id").where("user_id = ? and itematendimentos.quandoiraligar = ?","#{current_user.id}","#{amanhã.to_s[0..9]}").count
    @seuscontratos = Protocolo.where("idcorretor = ?","#{current_user.id}").count
    @atendimentocount = Atendimento.where("corretorresponsavel = ?","#{current_user.id}").group_by_day("updated_at").count
    #Administrador

    #Entradas
    #"2020-#{DateTime.now.to_s[5..6]}-1" "2020-#{DateTime.now.to_s[5..6]}-#{ultimodia}"
    @entradas_do_mes = Entrada.where("pago = 'Pago' and updated_at between ? and ?","2020-#{DateTime.now.to_s[5..6]}-1", "2020-#{DateTime.now.to_s[5..6]}-#{ultimodia}").sum(:valor)
    @entradas = (Entrada.where("pago = 'Pago'").sum(:valor))
    @entradasemaberto = Entrada.where("pago = 'Em aberto'").sum("valor")


    #Saidas
    @saidas_do_mes = Saida.where("pago = 'Pago' and updated_at between ? and ?","2020-#{DateTime.now.to_s[5..6]}-1", "2020-#{DateTime.now.to_s[5..6]}-#{ultimodia}").sum(:valor)
    @saidas = Saida.where("pago = 'Pago'").sum("valor")
    @saidasemaberto = Saida.where("pago = 'Em aberto'").sum("valor")

    @protocolos = Protocolo.joins("join statusprotocolos on statusprotocolos.protocolo_id=protocolos.id ").where("progress <> '100' and progress <> '0'")
    @solicitacoes = Solicitacoe.where("finalizado <> 'Sim'").order("created_at")
    @andamentocontratos = Protocolo.joins("join statusprotocolos on statusprotocolos.protocolo_id=protocolos.id ").where("progress <> '100' and progress <> '0'").count

    @totalpago = Saida.where("pago = 'Pago'").sum("valor")

    @totalpagomês = Saida.where("pago = 'Pago'").group_by_day("updated_at").count
    @totalinativomês = Saida.where("pago = 'Inativo'").group_by_day("updated_at").count

    @totalinativo = Saida.where("pago = 'Inativo'").sum("valor")
    @totalextorno = Saida.where("pago = 'Extorno'").sum("valor")

    @totalpagoe = Entrada.where("pago = 'Pago'").sum("valor")
    @totalinativoe = Entrada.where("pago = 'Inativo'").sum("valor")
    @totalextornoe = Entrada.where("pago = 'Extorno'").sum("valor")

    @contratos = Protocolo.joins("join statusprotocolos on statusprotocolos.protocolo_id=protocolos.id ").all.group_by_day("protocolos.created_at").count
    @contratosand = Protocolo.joins("join statusprotocolos on statusprotocolos.protocolo_id=protocolos.id ").where("progress <> '100' and progress <> '0'").group_by_day("protocolos.created_at").count
  end
end

class MovfinsController < ApplicationController
  before_action :set_movfin, only: [:show, :edit, :update, :destroy]

  # GET /movfins
  # GET /movfins.json
  def index
    #Gerar PDF
    if params[:dataini] and params[:datafin] and params[:tipo] and params[:gerar_pdf]
      @movfins = Movfin.where("created_at between ? and ? and tipo = ?","#{params[:dataini]}","#{params[:datafin]}","#{params[:tipo]}")
      @totalmov = Movfin.where("created_at between ? and ? and tipo = ?","#{params[:dataini]}","#{params[:datafin]}","#{params[:tipo]}").sum("valor")
        render pdf:"Movfin",
        page_size: 'A4',
        template: "movfins/index.pdf.erb",
        layout:"pdf.html",
        lowquality: true,
        zoon: 1,
        dpi: 75
    end  

    if params[:datainisearch] and params[:datafinsearch] and params[:tiposearch]
      @movfins = Movfin.where("created_at between ? and ? and tipo = ?","#{params[:datainisearch]}","#{params[:datafinsearch]}","#{params[:tiposearch]}")
      @totalmov = Movfin.where("created_at between ? and ? and tipo = ?","#{params[:datainisearch]}","#{params[:datafinsearch]}","#{params[:tiposearch]}").sum("valor")
    else
      @movfins = Movfin.all
      @totalmov = Movfin.all.sum("valor")
    end  
  end

  # GET /movfins/1
  # GET /movfins/1.json
  def show
  end

  # GET /movfins/new
  def new
    @movfin = Movfin.new
  end

  # GET /movfins/1/edit
  def edit
  end

  # POST /movfins
  # POST /movfins.json
  def create
    @movfin = Movfin.new(movfin_params)

    respond_to do |format|
      if @movfin.save
        format.html { redirect_to @movfin, notice: 'Movfin was successfully created.' }
        format.json { render :show, status: :created, location: @movfin }
      else
        format.html { render :new }
        format.json { render json: @movfin.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /movfins/1
  # PATCH/PUT /movfins/1.json
  def update
    respond_to do |format|
      if @movfin.update(movfin_params)
        format.html { redirect_to @movfin, notice: 'Movfin was successfully updated.' }
        format.json { render :show, status: :ok, location: @movfin }
      else
        format.html { render :edit }
        format.json { render json: @movfin.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /movfins/1
  # DELETE /movfins/1.json
  def destroy
    @movfin.destroy
    respond_to do |format|
      format.html { redirect_to movfins_url, notice: 'Movfin was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_movfin
      @movfin = Movfin.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def movfin_params
      params.require(:movfin).permit(:idmov, :tipo, :acao, :valor)
    end
end

class EntradasController < ApplicationController
  before_action :set_entrada, only: [:show, :edit, :update, :destroy]

  # GET /entradas
  # GET /entradas.json
  def index
    if params[:datainisearch] and params[:datafinsearch]
      @entradas = Entrada.where("dataentrada between ? and ?","#{params[:datainisearch]}","#{params[:datafinsearch]}").order(:updated_at)  
      @totalpago = Entrada.where("dataentrada between ? and ? and pago = 'Pago'","#{params[:datainisearch]}","#{params[:datafinsearch]}").sum("valor")
      @totalinativo = Entrada.where("dataentrada between ? and ?  and pago = 'Inativo'","#{params[:datainisearch]}","#{params[:datafinsearch]}").sum("valor")
      @totalextorno = Entrada.where("dataentrada between ? and ?  and pago = 'Extorno'","#{params[:datainisearch]}","#{params[:datafinsearch]}").sum("valor")
    else
      @entradas = Entrada.all.order(:updated_at)
      @totalpago = Entrada.where("pago = 'Pago'").sum("valor")
      @totalinativo = Entrada.where("pago = 'Inativo'").sum("valor")
      @totalextorno = Entrada.where("pago = 'Extorno'").sum("valor")
    end

    if params[:datainipdf] and params[:datafinpdf] and params[:gerar_pdf]
      @entradas = Entrada.where("dataentrada between ? and ?","#{params[:datainipdf]}","#{params[:datafinpdf]}")
      render pdf:"Entradas",
    	page_size: 'A4',
    	template: "entradas/index.pdf.erb",
    	layout:"pdf.html",
    	lowquality: true,
    	zoon: 1,
      dpi: 75
    end
  end

  # GET /entradas/1
  # GET /entradas/1.json
  def show
  end

  # GET /entradas/new
  def new
    @bancos = Banco.all
    @entrada = Entrada.new
  end

  # GET /entradas/1/edit
  def edit
    @bancos = Banco.all
  end

  # POST /entradas
  # POST /entradas.json
  def create
    @entrada = Entrada.new(entrada_params)
    Log.create(frase: 'Lançou uma Entrada',user_id: current_user.id,form: 'ENTRADA',idacao: "#{Entrada.all.pluck("max(id)").first + 1}")
    respond_to do |format|
      if @entrada.save
        format.html { redirect_to @entrada, notice: 'Entrada was successfully created.' }
        format.json { render :show, status: :created, location: @entrada }
      else
        format.html { render :new }
        format.json { render json: @entrada.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /entradas/1
  # PATCH/PUT /entradas/1.json
  def update
    Log.create(frase: 'Alterou uma Entrada',user_id: current_user.id,form: 'ENTRADA',idacao: @entrada.id)
    respond_to do |format|
      if @entrada.update(entrada_params)
        format.html { redirect_to @entrada, notice: 'Entrada was successfully updated.' }
        format.json { render :show, status: :ok, location: @entrada }
      else
        format.html { render :edit }
        format.json { render json: @entrada.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /entradas/1
  # DELETE /entradas/1.json
  def destroy
    @entrada.destroy
    respond_to do |format|
      format.html { redirect_to entradas_url, notice: 'Entrada was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_entrada
      @entrada = Entrada.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def entrada_params
      params.require(:entrada).permit(:banco_id, :dataentrada, :resumooperacao, :valor, :pago, :idcorretor, :idgerente, :cliente_id,:comprovante)
    end
end

class FeedbacktologsController < ApplicationController
  before_action :set_feedbacktolog, only: [:show, :edit, :update, :destroy]

  # GET /feedbacktologs
  # GET /feedbacktologs.json
  def index
    @feedbacktologs = Feedbacktolog.all
  end

  # GET /feedbacktologs/1
  # GET /feedbacktologs/1.json
  def show
  end

  # GET /feedbacktologs/new
  def new
    @feedbacktolog = Feedbacktolog.new
  end

  # GET /feedbacktologs/1/edit
  def edit
  end

  # POST /feedbacktologs
  # POST /feedbacktologs.json
  def create
    @feedbacktolog = Feedbacktolog.new(feedbacktolog_params)

    respond_to do |format|
      if @feedbacktolog.save
        format.html { redirect_to @feedbacktolog, notice: 'Feedbacktolog was successfully created.' }
        format.json { render :show, status: :created, location: @feedbacktolog }
      else
        format.html { render :new }
        format.json { render json: @feedbacktolog.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /feedbacktologs/1
  # PATCH/PUT /feedbacktologs/1.json
  def update
    respond_to do |format|
      if @feedbacktolog.update(feedbacktolog_params)
        format.html { redirect_to @feedbacktolog, notice: 'Feedbacktolog was successfully updated.' }
        format.json { render :show, status: :ok, location: @feedbacktolog }
      else
        format.html { render :edit }
        format.json { render json: @feedbacktolog.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /feedbacktologs/1
  # DELETE /feedbacktologs/1.json
  def destroy
    @feedbacktolog.destroy
    respond_to do |format|
      format.html { redirect_to feedbacktologs_url, notice: 'Feedbacktolog was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_feedbacktolog
      @feedbacktolog = Feedbacktolog.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def feedbacktolog_params
      params.require(:feedbacktolog).permit(:obs, :user_id, :log_id, :lido)
    end
end

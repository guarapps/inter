class ApplicationController < ActionController::Base
  before_action :authenticate_user!
  require 'brazilian-rails'


  def gerar_noticias(current_user_id,mensagem,grupo_destino,form,acao_id,lido)        
    case grupo_destino    
      when '0'
        user = User.joins("join perms on users.id=perms.user_id").where(" perms.cargo = '0'").pluck("users.id")
      when '1'
        user = User.joins("join perms on users.id=perms.user_id").where(" perms.cargo = '1'").pluck("users.id")
      when '2'
        user = User.joins("join perms on users.id=perms.user_id").where(" perms.cargo = '2'").pluck("users.id")
      when '3'
        user = User.joins("join perms on users.id=perms.user_id").where(" perms.cargo = '3'").pluck("users.id")            
    end

    case grupo_destino
      when '0'
        msg = " para os Admin"
      when '1'
        msg = " para os Operadores"
      when '2'
        msg = " para os Gerentes"
      when '4'
        msg = " para os Corretores"
    end
                  
    user.join(",").split(",").each do |userid|      
      Notice.create(user_id: current_user_id,
                    user_destino: userid,
                    mensagem: mensagem + msg,
                    grupo_destino: grupo_destino,
                    form: form,
                    acao_id:acao_id,
                    lido:lido)
    end
  end 
end

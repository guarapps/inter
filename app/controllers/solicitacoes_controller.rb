class SolicitacoesController < ApplicationController
  before_action :set_solicitacoe, only: [:show, :edit, :update, :destroy]

  # GET /solicitacoes
  # GET /solicitacoes.json
  def index
    case Perm.where("user_id = ?","#{current_user.id}").pluck('cargo').first
    when '3'
    @solicitacoes = Solicitacoe.where("finalizado <> 'Sim' and user_id = ?","#{current_user.id}").order("created_at desc")
    when '2'
    @solicitacoes = Solicitacoe.where("finalizado <> 'Sim' and user_id = ?","#{current_user.id}").order("created_at desc")    else
    @solicitacoes = Solicitacoe.where("finalizado <> 'Sim'").order("created_at desc")
    end
  end

  # GET /solicitacoes/1
  # GET /solicitacoes/1.json
  def show
    if params[:finalizar]
      Solicitacoe.where(id: params[:id]).update(finalizado: 'Sim')
    end
  end

  # GET /solicitacoes/new
  def new
    @solicitacoe = Solicitacoe.new
  end

  # GET /solicitacoes/1/edit
  def edit
  end

  # POST /solicitacoes
  # POST /solicitacoes.json  user:references user_destino:integer lido:string grupo_destino:integer mensagem:text acao_id:integer form:string
  def create
    @solicitacoe = Solicitacoe.new(solicitacoe_params)  
      
    gerar_noticias(current_user.id,'Enviou uma solicitação',solicitacoe_params[:userdestino],'solicitacoes',"#{Solicitacoe.all.pluck("max(id)").first + 1}","N")
    
    Log.create(frase: 'Criou uma Solicitação',user_id: current_user.id,form: 'SOLICITAÇÃO',idacao: "#{Solicitacoe.all.pluck("max(id)").first + 1}")
    respond_to do |format|
      if @solicitacoe.save
        format.html { redirect_to @solicitacoe, notice: 'Solicitacoe was successfully created.' }
        format.json { render :show, status: :created, location: @solicitacoe }
      else
        format.html { render :new }
        format.json { render json: @solicitaco.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /solicitacoes/1
  # PATCH/PUT /solicitacoes/1.json
  def update
    Notice.create(user_id: current_user.id, mensagem:'Alterou uma solicitação',grupo_destino: @solicitacoe.userdestino, form: 'solicitacoes',acao_id:@solicitacoe.id,lido: 'N')
    Log.create(frase: 'Alterou uma Solicitação',user_id: current_user.id,form: 'SOLICITAÇÃO',idacao: @solicitacoe.id)
    respond_to do |format|
      if @solicitacoe.update(solicitacoe_params)
        format.html { redirect_to @solicitacoe, notice: 'Solicitacoe was successfully updated.' }
        format.json { render :show, status: :ok, location: @solicitaco }
      else
        format.html { render :edit }
        format.json { render json: @solicitaco.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /solicitacoes/1
  # DELETE /solicitacoes/1.json
  def destroy
    @solicitacoe.destroy
    respond_to do |format|
      format.html { redirect_to solicitacoes_url, notice: 'Solicitacoe was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_solicitacoe
      @solicitacoe = Solicitacoe.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def solicitacoe_params
      params.require(:solicitacoe).permit(:titulo, :dataabertura, :finalizado, :user_id, :userdestino)
    end
end

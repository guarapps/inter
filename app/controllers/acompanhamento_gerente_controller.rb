class AcompanhamentoGerenteController < ApplicationController
  def index
    @gerente = User.joins("join perms on users.id=perms.user_id").where("perms.cargo = '2' and idgerente = ?","#{current_user.id}")
  end
end

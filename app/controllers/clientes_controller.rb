class ClientesController < ApplicationController
  before_action :set_cliente, only: [:show, :edit, :update, :destroy]

  # GET /clientes
  # GET /clientes.json
  def index

    case Perm.where("user_id = ?","#{current_user.id}").pluck('cargo').first
    when '3'
      if @clientes = Cliente.where("user_id = ?","#{current_user.id}").present?
        @clientes = Cliente.where("user_id = ?","#{current_user.id}").order("nome asc") 
        if params[:nome]
          @clientes = Cliente.where("nome ilike ? and user_id = ?","%#{params[:nome]}%","#{current_user.id}").order("nome asc") 
        elsif params[:cpf]
          @clientes = Cliente.where("cpf like ? and user_id = ?","%#{params[:cpf]}%","#{current_user.id}").order("nome asc") 
        else
          @clientes = Cliente.where("user_id = ?", "#{current_user.id}").order("nome asc") 
        end
      else
        flash[:notice] =  "Não há Clientes!"
        @clientes = Cliente.where("user_id = ?","#{current_user.id}").order("nome asc") 
      end
    when '2'
      if @clientes = Cliente.where("user_id = ?","#{current_user.id}").present?
        @clientes = Cliente.where("user_id = ?","#{current_user.id}").order("nome asc") 
        if params[:nome]
          @clientes = Cliente.where("nome ilike ? and user_id = ?","%#{params[:nome]}%","#{current_user.id}").order("nome asc") 
        elsif params[:cpf]
          @clientes = Cliente.where("cpf like ? and user_id = ?","%#{params[:cpf]}%","#{current_user.id}").order("nome asc") 
        else
          @clientes = Cliente.where("user_id = ?", "#{current_user.id}").order("nome asc") 
        end
      else
        flash[:notice] =  "Não há Clientes!"
        @clientes = Cliente.where("user_id = ?","#{current_user.id}").order("nome asc") 
      end
    else
      @clientes = Cliente.all 
      if params[:nome]
        @clientes = Cliente.where("nome ilike ? ","%#{params[:nome]}%").order("nome asc") 
      elsif params[:cpf]
        @clientes = Cliente.where("cpf like ? ","%#{params[:cpf]}%").order("nome asc") 
      else
        @clientes = Cliente.all.order("nome asc")
    end
end
  end

  # GET /clientes/1
  # GET /clientes/1.json
  def show
    @notice = Notice.new
    documento_id = Documento.select(:id).where(cliente_id: params[:id]).pluck(:id)
    @documentos = Documento.find(documento_id)    
    @users = User.all
    #@items =
  end

  # GET /clientes/new
  def new
    @cliente = Cliente.new
  end

  # GET /clientes/1/edit
  def edit
  end

  # POST /clientes
  # POST /clientes.json
  def create
    @cliente = Cliente.new(cliente_params)
    Log.create(frase: 'Cadastrou um novo Cliente!',user_id: current_user.id,form: 'CLIENTE',idacao: "#{Cliente.all.pluck("max(id)").first + 1}")
    respond_to do |format|
      if @cliente.save
        format.html { redirect_to @cliente, notice: 'Cliente was successfully created.' }
        format.json { render :show, status: :created, location: @cliente }
      else
        format.html { render :new }
        format.json { render json: @cliente.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /clientes/1
  # PATCH/PUT /clientes/1.json
  def update
    Log.create(frase: 'Alterou o cadastro de um Cliente',user_id: current_user.id,form: 'CLIENTE',idacao: @cliente.id)
    respond_to do |format|
      if @cliente.update(cliente_params)
        format.html { redirect_to @cliente, notice: 'Cliente was successfully updated.' }
        format.json { render :show, status: :ok, location: @cliente }
      else
        format.html { render :edit }
        format.json { render json: @cliente.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /clientes/1
  # DELETE /clientes/1.json
  def destroy
    @cliente.destroy
    respond_to do |format|
      format.html { redirect_to clientes_url, notice: 'Cliente was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_cliente
      @cliente = Cliente.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def cliente_params
      params.require(:cliente).permit(:nome, :cpf, :tel, :matricula, :DataNascimento, :Obs, :senhaccheque,:user_id,:atendimento_id)
    end
end

Rails.application.routes.draw do

  resources :notices
  resources :documentos
  resources :feedbacktologs
  get 'acompanhamento_gerente/index'
  resources :feedbacks
  resources :posts
  resources :logs
  resources :itematendimentoclientes
  get 'ranking/index'
  resources :itematendimentos
  resources :atendimentos do
    resources :itematendimentos
    resources :clientes
  end
  resources :itemsolics
  resources :solicitacoes do
    resources :itemsolics
  end
  resources :movfins
  resources :saidas
  resources :entradas
  get 'relacaocontratos/index'
  resources :statusprotocolos do
    resources :saidas
  end
  resources :perms
  resources :dados
  devise_for :users, :path_prefix => 'login'
  resources :users do
      resources :perms
      resources :dados
  end
  resources :permissoes
  resources :protocolos do
    resources :feedbacks
    resources :statusprotocolos
  end
  resources :bancos
  resources :clientes do
    resources :documentos
    resources :feedbacks
    resources :itematendimentoclientes
    resources :entradas
    resources :protocolos do
      resources :feedbacks
      resources :statusprotocolos do
        resources :saidas
      end
    end
  end
  root 'paginainicial#index'
  get 'paginainicial/index'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end

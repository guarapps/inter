class Integer
  def to_dinheiro
    Dinheiro.new(self).real_formatado
  end

  def to_dinheiro_s_cifra
    Dinheiro.new(self).to_s
  end
end

class Float
  def to_dinheiro
    Dinheiro.new(self).real_formatado
  end

  def to_dinheiro_s_cifra
    Dinheiro.new(self).to_s
  end

  def r_dinheiro(size=15, filler='0')
    self.to_dinheiro_s_cifra.gsub(',','').gsub('.','').rjust(size,filler)
  end

  def r_dinheiro_com_ponto(size=15, filler='0')
    self.to_dinheiro_s_cifra.gsub(',','.').rjust(size,filler)
  end
end

class BigDecimal
  def to_dinheiro
    Dinheiro.new(self).real_formatado
  end

  def to_dinheiro_s_cifra
    Dinheiro.new(self).to_s
  end
end

class Fixnum
  def to_dinheiro
    Dinheiro.new(self).real_formatado
  end

  def to_dinheiro_s_cifra
    Dinheiro.new(self).to_s
  end
end

class NilClass
  def to_dinheiro
    ""
  end

  def to_dinheiro_s_cifra
    ""
  end
end
